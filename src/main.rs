extern crate substring;
use std::collections::HashMap;
use std::env;
use std::primitive::usize;
use substring::Substring;

const TITLE: &'static str = "maskitty-rs";
const VERSION: &'static str = "1.0.0";

/**
 * Build a map of hexadecimal symbols.
 */
fn build_hex_chars() -> HashMap<u16, String> {
  let mut charset: HashMap<u16, String> = HashMap::with_capacity(16);
  charset.insert(0, "0".to_string());
  charset.insert(1, "1".to_string());
  charset.insert(2, "2".to_string());
  charset.insert(3, "3".to_string());
  charset.insert(4, "4".to_string());
  charset.insert(5, "5".to_string());
  charset.insert(6, "6".to_string());
  charset.insert(7, "7".to_string());
  charset.insert(8, "8".to_string());
  charset.insert(9, "9".to_string());
  charset.insert(10, "A".to_string());
  charset.insert(11, "B".to_string());
  charset.insert(12, "C".to_string());
  charset.insert(13, "D".to_string());
  charset.insert(14, "E".to_string());
  charset.insert(15, "F".to_string());
  charset
}

/**
 * Split the option string on delimeter.
 */
fn split(option: &String, delimeter: char) -> Vec<String> {
  let mut res: Vec<String> = Vec::new();
  let options = option.split(delimeter).collect::<Vec<&str>>();
  for option in options {
    res.push(option.to_string());
  }
  res
}

/**
 * Expand a number range of N-M format.
 */
fn expand(splits: &Vec<String>) -> Vec<u16> {
  let mut res: Vec<u16> = Vec::new();
  for split in splits {
    let found_prim = split.find("-");
    if found_prim != None {
      let found = found_prim.unwrap();
      let mut i: u16 = split.substring(0, found).to_string().parse::<u16>().unwrap();
      let stop = split.substring(found + 1, split.len()).to_string().parse::<u16>().unwrap() + 1;
      while i < stop {
        res.push(i);
        i += 1;
      }
    } else {
      res.push(split.to_string().parse::<u16>().unwrap());
    }
  }
  res
}

/**
 * Get the max of expanded range.
 */
fn get_max(cores: &Vec<u16>) -> u16 {
  let mut max: u16 = cores[0];
  for core in cores {
    if core > &max {
      max = *core;
    }
  }
  max
}

/**
 * Parse cmd options string, get value.
 */
fn get_cmd_option(argc: u16, argv: &Vec<String>, soption: &str, loption: &str) -> String {
  let mut cmd: String = String::new();
  let mut i: u16 = 0;
  while i < argc {
    let mut arg = String::new();
    let mut j: u16 = 0;
    for ele in argv {
      if j == i {
        arg = (&ele).to_string();
        break;
      }
      j += 1;
    }
    if (arg.contains(soption)) || (arg.contains(loption)) {
      cmd = arg
        .substring(arg.find("=").unwrap() + (1 as usize), arg.len())
        .to_string();
      return cmd;
    }
    i += 1;
  }
  cmd
}

/**
 * Parse cmd options string, get bool of existance.
 */
fn is_cmd_option(argc: u16, argv: &Vec<String>, soption: &str, loption: &str) -> bool {
  let mut i: u16 = 0;
  while i < argc {
    let mut arg = String::new();
    let mut j: u16 = 0;
    for ele in argv {
      if j == i {
        arg = (&ele).to_string();
        break;
      }
      j += 1;
    }
    if (arg.contains(soption)) || (arg.contains(loption)) {
      return true;
    }
    i += 1;
  }
  false
}

/**
 * Calculate the codes for the cores specified.
 */
fn calc_codes(cores: u16) -> Vec<u64> {
  let mut codes: Vec<u64> = Vec::new();
  let mut prev: u64 = 1;
  codes.push(prev);
  let mut i: u16 = 0;
  while i < cores {
    prev = 2 * prev;
    codes.push(prev);
    i += 1;
  }
  codes
}

/**
 * Convert a decimal integer to hexadecimal string.
 */
fn int_to_hex(charset: &HashMap<u16, String>, mask: &u64) -> String {
  let mut mask_str = String::new();
  let mut q = *mask;
  let mut r: u16;
  while q > 15 {
    let q_old = q;
    q = q / 16;
    r = (q_old % 16) as u16;
    mask_str = charset.get(&r).unwrap().to_string() + &mask_str;
  }
  if q != 0 {
    mask_str = charset.get(&(q as u16)).unwrap().to_string() + &mask_str;
  }
  mask_str
}

fn main() {
  let args: Vec<String> = env::args().collect();
  if is_cmd_option(args.len() as u16, &args, "-h", "--help") {
    println!("{TITLE} v{VERSION}");
    println!("");
    println!("-m=/--mask= [RANGE]\tthe range of nums representing your affinity mask (eg 0-3,6)");
    println!("-t/--threads\t\treturn number of threads in output eg 'maskitty -m=0-5 -t' returns '6 3F'");
    println!("-h/--help\t\tprint this menu");
    return;
  }

  let hex = build_hex_chars();

  let mask_arg = get_cmd_option(args.len() as u16, &args, "-m=", "--mask=");
  let mask_range = expand(&split(&mask_arg, ','));
  let codes = calc_codes(get_max(&mask_range));

  let mut mask_dec: u64 = 0;
  for core in &mask_range {
    mask_dec += *codes.get(*core as usize).unwrap() as u64;
  }

  if is_cmd_option(args.len() as u16, &args, "-t", "--threads") {
    print!("{:?} ", mask_range.len());
  }
  let mask_hex = int_to_hex(&hex, &mask_dec);
  println!("{}", mask_hex);
}
